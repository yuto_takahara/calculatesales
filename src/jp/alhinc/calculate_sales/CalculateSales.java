package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_CONTINUE = "売上ファイル名が連番になっていません";
	private static final String TOTAL_OVER_10DIGITS = "合計金額が10桁を超えました";
	private static final String INVALID_FORMAT = "のフォーマットが不正です";
	private static final String INVALID_BRANCH_FORMAT = "の支店コードが不正です";
	private static final String INVALID_COMMODITY_FORMAT = "の商品コードが不正です";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//エラー処理
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String,String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "支店", "^[0-9]{3}$")) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "商品", "^[A-Za-z0-9]{8}$")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)


		File[] files = new File(args[0]).listFiles();
		List<File> rcdfiles = new ArrayList<>();
		for(int i = 0; i < files.length; i++){
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}\\.rcd$")) {
				//名前が一致したファイルを読み込む
				rcdfiles.add(files[i]);
			}
		}
		//エラー処理
		Collections.sort(rcdfiles);
		for(int i = 1; i < rcdfiles.size(); i++) {
			int former = Integer.parseInt(rcdfiles.get(i - 1).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdfiles.get(i).getName().substring(0, 8));
			if((latter - former) != 1){
				System.out.println(FILE_NOT_CONTINUE);
				return;
			}
		}

		BufferedReader br = null;
		for(int i = 0; i < rcdfiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdfiles.get(i));
				br = new BufferedReader(fr);
				ArrayList<String> rcdsales = new ArrayList<>();
				String line;
				// 一行ずつ読み込む
				while((line = br.readLine()) != null) {
					rcdsales.add(line);
				}
				//エラー処理
				if(rcdsales.size() != 3){
					System.out.println(rcdfiles.get(i).getName() + INVALID_FORMAT);
					return;
				}
				//エラー処理
				if(!branchSales.containsKey(rcdsales.get(0))) {
					System.out.println(rcdfiles.get(i).getName() + INVALID_BRANCH_FORMAT);
					return;
				}
				//エラー処理
				if(!commoditySales.containsKey(rcdsales.get(1))) {
					System.out.println(rcdfiles.get(i).getName() + INVALID_COMMODITY_FORMAT);
					return;
				}
				//エラー処理
				if(!rcdsales.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				//支店コードおよび商品コードと売上金額を保持するMapに追加する
				Long fileSale = Long.parseLong(rcdsales.get(2));
				//合計金額を入れる変数を宣言
				Long saleAmountBranch = fileSale + branchSales.get(rcdsales.get(0));
				Long saleAmountCommodity = fileSale + commoditySales.get(rcdsales.get(1));
				//エラー処理
				if(saleAmountBranch >= 10000000000L || saleAmountCommodity >= 10000000000L) {
					System.out.println(TOTAL_OVER_10DIGITS);
					return;
				}
				branchSales.put(rcdsales.get(0), saleAmountBranch);
				commoditySales.put(rcdsales.get(1), saleAmountCommodity);
			}
			catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			}
			finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					}
					catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> namesMap, Map<String, Long> salesMap, String name, String match) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//エラー処置
			if(!file.exists()) {
				System.out.println(name + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");
				//エラー処理
				if(items.length != 2 || !items[0].matches(match)) {
					System.out.println(name + FILE_INVALID_FORMAT);
					return false;
				}
				namesMap.put(items[0], items[1]);
				salesMap.put(items[0], 0L);
			}
		}
		catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		}
		finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				}
				catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> namesMap, Map<String, Long> salesMap) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : namesMap.keySet()) {
				bw.write(key + "," + namesMap.get(key) + "," + salesMap.get(key));
				bw.newLine();
			}
		}
		catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		}
		finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				}
				catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
